This repo contains list of packages in the Manjaro `extra` and `community`
repositories that have [probably] not yet been migrated from Github to GitLab.

## Detecting

These lists were created in a few steps.

**First**, a list of package folders was found in
https://github.com/manjaro/packages-community
and https://github.com/manjaro/packages-extra

**Second**, a list of projects was fetched from
https://gitlab.manjaro.org/packages/community and
https://gitlab.manjaro.org/packages/extra using the [GitLab
API](https://docs.gitlab.com/ee/api/groups.html). _(example to
fetch the first 100 projects in Community group in GitLab: `curl
https://gitlab.manjaro.org/api/v4/groups/22/projects?include_subgroups=yes&per_page=100`)_

**Third**, the delta of repos for each `community` and `extra` was created by
comparing the lists created above and detecting repositiores found in Github but
not in GitLab.

**Fourth**, each potential project to migrate was checked to see if a package
currently exists for installation. This was done with `pacman -Si`. The
name of the package was detected from the `PKGBUILD` of the project found in the
Github repo.

---

## Lists

Each list contains the package name (or base if detected), the URL for the
project as found in the `PKGBUILD` file, the name of the directory in the Github
repository, and a link to the folder containing the package in Github.

**Migrate:** Projects that were found in Github and not in GitLab and that had a
current package that could be installed were added to the `migrate` lists. These
are the projects that likely need to be migrated to GitLab but should be
reviewed to make sure moving then over would be worthwhile or that they haven't
already been migrated with a different name.

> _When I created these lists there were 146 packages in `community` and 53 in
> `extra` that [probably] need to be migrated._

**Missing:** Projects that were found in Github and not in Gitlab but that did
_not_ seem to have an installable package in the repository were added to the
`missing` lists. These should probably be reviewed for mistakes and can likely
be ignored if nobody wants to move them over.

> _When I created these lists there were 46 packages in `community` and 25 in
> `extra` that did not appear to have a corresponding package that could be
> installed via pacman and so [probably] don't need to be migrated._

---

## Migrate

### [Community](https://gitlab.com/zaxmyth/manjaro-gitlab-migration/blob/master/packages-community-migrate.txt)

- alpm-octopi-utils
- arc-maia-icon-theme
- awesome-wallpapers
- bar-aint-recursive
- bdf-zevv-peep
- brandr
- c2esp
- cbatticon-gtk2
- cmst
- compare-mirrors
- conky-cli
- copyq
- daala
- delayed-hibernation
- deluge
- disable-tracker
- econnman
- emacs-speechd-el
- espeak
- faba-icon-theme
- fbmenugen
- fbmenu-manjaro
- fisherman
- frobtads
- frotz
- grub-btrfs
- gtk2-compatibility
- gtk-theme-breath
- gtk-theme-config
- gtk-theme-e17gtk
- gtk-theme-maia
- gtk-theme-numix-solarized
- gtk-theme-stylishdark
- hal-flash
- hibernator
- imagewriter
- jwm
- kde1-kdebase
- kde1-kdelibs
- kde-servicemenus-pkg-tools
- kde-servicemenus-rootactions
- keyboardctl
- lemonbar-xft
- libpthread-stubs
- light
- lightdm-another-gtk-greeter
- lightdm-webkit2-greeter-manjaro
- lighter-gnome
- locale-info
- lua-clock-manjaro
- lumina-desktop
- lxappearance-gtk3
- lxdm
- lxmed
- lxmusic-gtk3
- lxsession
- manjaro-architect
- manjaro-architect-dev
- manjaro-aur-support
- manjaro-gnome-assets
- manjaro-lumina-settings
- manjaro-xdg-menu
- manjaro-zsh-config
- manj-grub
- markdown_previewer
- mdsplib
- menda-lxqt-panel
- mhwd-chroot
- mhwd-tui
- moka-icon-theme
- morc_menu
- muser-wallpapers
- mygtkmenu
- mygtkmenui
- mypaint-brushes
- nautilus-empty-file
- nerd-fonts-terminus
- networkmanager-dmenu
- noto-fonts-compat
- obkey
- oblogout-manjaro
- obmenu-generator
- ocrdesktop
- open-fuse-iso
- open-fuse-iso-term
- oysttyer
- pantheon-wallpaper
- parole
- pcaudiolib
- pdf2img-c
- pekwm-menu
- pkgcacheclean
- pmenu
- preload
- prpltwtr
- pulseaudio-equalizer-ladspa
- py-corners
- pyplanemode
- python-efl
- python-svgwrite
- python-xapp
- qpdfview
- qt1
- qt5gtk2
- qt-at-spi
- raktpdf
- sakis3g
- sddm-classic
- sidplay2-libs
- skippy-xd
- slack-online
- snapper-gui
- sndio
- spacefm
- srandrd
- steam-manjaro
- steam-native
- sunflower
- sxiv-rifle
- terminus-font
- timeset
- timeset-gui
- tintin
- tintin-alteraeon
- tintwizard
- tlp
- tmpwatch
- udevil
- udiskie
- vicky
- volnoti
- volumeicon-gtk2
- wicd-patched
- wpa_supplicant_gui
- wpa_tui
- xapps
- xboomx
- xcmenu
- xdgmenumaker
- xdo
- xfce4-weather-plugin-menda-circle-icons
- xlogin
- xprintidle
- xtitle
- xwinfo
- zensu

## [Extra](https://gitlab.com/zaxmyth/manjaro-gitlab-migration/blob/master/packages-extra-migrate.txt)

- brltty
- catalyst-input
- catalyst-server
- catalyst-utils
- catalyst-video
- cogl
- cower
- devtools
- dkms
- exo
- firefox
- garcon
- gfxboot
- gnutls28
- kpmcore
- libxfont
- libxpresent
- manjaro-artwork
- manjaro-backgrounds
- manjaro-base-skel
- manjaro-documentation
- manjaro-documentation-fr
- manjaro-firmware
- manjaro-icons
- manjaro-printer
- manjaro-settings-manager-dev
- manjaro-wallpapers-17.0
- mkinitcpio-openswap
- nvidia-340xx-utils
- package-query
- pam_encfs
- plymouth-theme-manjaro
- plymouth-theme-manjaro-elegant
- plymouth-theme-manjaro-elegant-hidpi
- ruby-hashie
- ruby-json
- ruby-kwalify
- ruby-osdn-cli
- ruby-osdn-client
- ruby-typhoeus
- samba
- thunar
- thunderbird
- ttf-aboriginal-sans
- ttf-lohit-fonts
- ttf-myanmar3
- ttf-thaana-fonts
- xcompmgr
- xdg-su
- xfce4-session
- xfce4-volumed-pulse
- xfconf
- xfdesktop

---

## Missing

### [Community](https://gitlab.com/zaxmyth/manjaro-gitlab-migration/blob/master/missing/packages-community-missing.txt)

- adwaita-manjaro-themes
- alternating-layouts
- arc-themes-manjaro
- artwork-andromeda
- audacious-gtk3
- audacious-plugins-gtk3
- basilisk-bin
- chromium-chromevox
- desktop-settings
- engrampa-thunar-plugin
- fetter
- gimp-devel
- grub2-theme-manjaro-redefined
- jade-dashboard-git
- jade-menu-data-git
- kde-servicemenus-dropbox
- linux416-rt
- linux-tools
- manjarobox-artwork
- manjaro-browser-settings-native
- manjaro-fluxbox-settings
- manjaro-gnome-assets-dev
- manjaro-gnome-maia-theme
- manjaro-gnome-settings-v17
- manjaro-gnome-settings-v18
- manjaro-lxqt-settings
- manjaro-webdad-settings-git
- ms-office-online
- nautilus-typeahead
- pacli
- pamac-classic
- pocketsphinx
- sddm-manjaro-theme
- solarized-dark-themes
- sonarlinux-profiles
- sphinxbase
- sublime-text-dev
- virtualbox-dkms-modules
- webdad-speech-git
- webdad-theming-git
- webdad-webapps-git
- webdad-workspaces-git
- xfce4-dockbarx-plugin
- xfce4-panel-compiz
- xfce-theme-greenbird
- yaourt-gui-manjaro

### [Extra](https://gitlab.com/zaxmyth/manjaro-gitlab-migration/blob/master/missing/packages-extra-missing.txt)

- bootsplash-themes
- gtk-xfce-engine
- kpmcore-dev
- manjaro-features-dev
- manjaro-gdm-check
- manjaro-meta
- mesa-libgl
- mesa-wayland-dri
- nvidia-304xx-utils
- nvidia-utils
- pamac-dev
- pamac-exp
- partitionmanager-dev
- plasma-agents
- plymouth-theme-sonar-elegant
- python-pykwalify
- sonar-artworks
- sonar-desktop-settings
- sonar-isolinux
- sonar-iso-profiles
- syslinux-themes
- thus
- thus-dev
- xfce4-volumed
- yaourt
