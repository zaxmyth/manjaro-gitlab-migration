#!/usr/bin/bash

# migrate.sh packages-extra-migrate.txt ~/src/migrated-repos ~/src/manjaro-github migrate-manjaro-extra 6152866 > extra-migration.log 2>&1

LIST=$1
DEST=$2
GHDIR=$3
GROUP=$4
GROUP_ID=$5 #extra=6152866, community=6153543

WORKING=`pwd`

TOKEN="Q6938U1o8BTgxA7ssWPB"

for p in $(cat $LIST)
do
    cd $WORKING

    IFS=',' read -ra PKG <<< "$p"
    eval "${PKG[2]};${PKG[3]}" # dir=, github=

    name=$(basename $dir)

    cp -r $GHDIR/$dir $DEST/$(dirname $dir)

    cd $DEST/$dir

    git init .
    git add .
    git commit -m "Migrated from $github"

    # create the remote repository
    curl --header "PRIVATE-TOKEN: $TOKEN" -X POST https://gitlab.com/api/v4/projects\?name\=$name\&namespace_id\=$GROUP_ID\&visibility\=public

    git remote add origin git@gitlab.com:$GROUP/$name.git
    git push -u origin master
done
